#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
//ф-я , яка заповнює массив випадковими значеннями
void random2(int *a,int n,int m){
	int i,j;
	printf("Вхідний масив :\n");
	for(i=0 ; i<n ; i++){
		for(j=0; j<m ; j++){
			*(a+ i*n + j)=rand()%100-50;
			printf("%4d",*(a+ i*n + j));
			
		}
		cout<<endl;
	}
}

void search(int mas[], int size, int ipos){
	int i;
	for(i=size-1;i>=0;i--){
		if(mas[i]<0){
			printf("\n Останнє від'ємне число =%4d\n j=%4d\n i=%4d\n",mas[i],i,ipos);
			return;
		}
	}
}

int main() {
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n,m,i,j;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a[n][m];
	int b[m];
	random2(a[0],n,m);
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			b[j]=a[i][j];
		}
		search(b,m,i);
	}

	cout<<endl;
	system("pause");
	return 0;
}
